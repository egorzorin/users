import data from "@/domain/Users/data/content.json";
import { ContentType } from "@/domain/Users/Users.types";
class Users {
  getContent(): Promise<ContentType[]> {
    return new Promise((resolve, reject) => {
      try {
        setTimeout(() => {
          resolve(data);
        }, 1000);
      } catch (e) {
        reject(e);
      }
    });
  }
}

export const usersService = new Users();
