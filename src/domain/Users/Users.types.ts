export type ScoresType = number;
export type UserType = {
  avatar: string;
  title: string;
  subtitle: string;
  country: string;
  scores: ScoresType;
};

export type FilterTypePayload = { field: string; value: unknown };

export type FilterType = Record<string, unknown>;

export type ContentType = Partial<
  UserType & {
    divider: boolean;
    inset: boolean;
    header: string;
  }
>;
