import { FilterType, FilterTypePayload } from "@/domain/Users/Users.types";
import { Commit, createStore } from "vuex";

type FiltersState = {
  filters: FilterType;
};

const filtersModule = {
  namespaced: true,
  state: (): FiltersState => ({
    filters: {},
  }),
  mutations: {
    changeFilters(state: FiltersState, filter: FilterTypePayload) {
      const [entries] = Object.entries(filter);
      if (!Object.values(filter).join("")) {
        delete state.filters[entries[0]];
        return;
      }
      state.filters[entries[0]] = entries[1];
    },
  },
  actions: {
    changeFilters({ commit }: { commit: Commit }, filter: FilterTypePayload) {
      commit("changeFilters", filter);
    },
  },
};

export default createStore({
  modules: {
    filtersModule: filtersModule,
  },
});
